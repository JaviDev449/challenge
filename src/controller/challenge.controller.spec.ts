import request = require("supertest");
import app from '../app'

describe('GET challenge service', () => {
  it('respond with server work it! ', async () => {
    const result = await request(app).get("/");
    expect(result.statusCode).toEqual(200);   
    expect(result.text).toEqual("server work it!");
  }) 

  it('respond with a arry of length equals to 100', async () => {
    const result = await request(app).get("/challenge");
    expect(result.statusCode).toEqual(200); 
    expect(result.body.response.length).toEqual(100);
  })

})

