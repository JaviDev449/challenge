
function serverWorkit(req, res): string {
  return res.status(200).send('server work it!')
}

function getChallenge(req, res): Array<string> {

  const response = new Array();
  const filtersPrint = ['Musical','Music','TI']; 
  const filtersType = [15,3,5];

  let index = 1;
  let data = [...Array(100)].map(() => index++);

  function getData(option: number): string {
    for (let i = 0; i <= filtersType.length; i++) {
      if (option % filtersType[i] === 0) return filtersPrint[i]
    }
    return option.toString();
  }

  data.forEach(number => {
    response.push(getData(number))
  })

  return res.status(200).send({response})
}

export const controllerChallenge = {
  getChallenge,
  serverWorkit,
}
