import express from 'express'
import { controllerChallenge } from '../controller/challenge.controller'

const challengeRoute = express.Router();
challengeRoute.get('/',controllerChallenge.serverWorkit);
challengeRoute.get('/challenge', controllerChallenge.getChallenge);


export default [
  challengeRoute
]
