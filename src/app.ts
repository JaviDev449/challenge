import express from 'express'
import { challengeRoute } from './routes/index'

const app = express();
const port = 3001;

app.use(express.json());
app.use(challengeRoute);

app.listen(port, () => {
  console.log(`listen on port ${port}`);
});


export default app